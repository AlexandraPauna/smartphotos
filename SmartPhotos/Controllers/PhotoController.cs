﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SmartPhotos;
using SmartPhotos.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProiectDaw.Controllers
{
    public class PhotoController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Photo
        public ActionResult Index(string searchString)
        {
            var images = from photo in db.Photos
                         select photo;
            bool searched = false;
            bool likesPresent = false;
            // Tuple<NumeCategorie, NrLike-uri, Procent>
            List<Tuple<string, int, int>> categories = new List<Tuple<string, int, int>>();
            if (!String.IsNullOrEmpty(searchString))
            {
                searched = true;
                images = images.Where(s => s.Title.Contains(searchString) || s.Caption.Contains(searchString) || s.AltText.Contains(searchString));
            } else if (User.Identity.GetUserId() != null) {
                var cat = from category in db.Categories
                          select category.Id;
                string userId = User.Identity.GetUserId();
                int cnt = 0;
                var likeuri = from likes in db.Likes
                              where likes.UserId == userId
                              select likes.LikeId;
                int nrOfLikes = likeuri.Count();
                if (nrOfLikes != 0)
                {
                    likesPresent = true;
                    foreach (var ct in cat)
                    {
                        var li = from likes in db.Likes
                                 where likes.UserId == userId && likes.isLike
                                 select likes.LikeId;
                    
                        //li = li.Where
                        foreach(var l in li)
                        {
                            var c = from likes in db.Likes
                                    where likes.LikeId == l
                                    select likes.CategoryId;
                            List<int> auxIntList2 = c.ToList();
                            int auxint2 = auxIntList2.ElementAt(0);

                            var cn = from categ in db.Categories
                                     where categ.Id == auxint2
                                     select categ.Id;
                            List<int> auxIntList = new List<int>();
                            foreach (var cid in cn)
                            {
                                auxIntList.Add(cid);
                            }
                            if (auxIntList.ElementAt(0) == ct)
                            {
                                cnt++;
                            }
                        }
                        categories.Add(new Tuple<string, int, int>(ct.ToString(), cnt, (cnt*100/nrOfLikes)));
                        nrOfLikes = nrOfLikes + cnt;
                        cnt = 0;
                    }
                
                    // Sortam categoriile dupa nr de like-uri pe care user-ul le-a acordat
                    categories.Sort((x, y) => -(x.Item2.CompareTo(y.Item2)));
                }
            }

            List<Photo> imagesArray = new List<Photo>();
            if (searched || !likesPresent)
            {
                images = images.OrderByDescending(i => i.CreatedDate);
            }
            else
            {
                // sortare dupa nr de like-uri
                var auxImg = images;
                List<Photo> auxList = new List<Photo>();

                foreach (Photo p in auxImg)
                {
                    auxList.Add(p);
                }

                while (auxList.Count() != 0)
                {
                    foreach (Tuple<string, int, int> t in categories)
                    {
                        int nrOfPhotos = t.Item3 / 10;
                        if (nrOfPhotos < 1)
                        {
                            nrOfPhotos = 1;
                        }
                        for ( int i = 0; i < nrOfPhotos; i++)
                        {
                            for ( var j = 0; j < auxList.Count(); j++)
                            {
                                if (auxList.ElementAt(j).IdCategory.ToString().Equals(t.Item1))
                                {
                                    imagesArray.Add(auxList.ElementAt(j));
                                    auxList.RemoveAt(j);
                                    break;
                                }
                            }
                            if (auxList.Count() == 0)
                            {
                                break;
                            }
                        }
                        if (auxList.Count() == 0)
                        {
                            break;
                        }
                    }
                }
                
            }
            var img = db.Photos.Include("Category").Include("User");
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            ViewBag.userCurent = User.Identity.GetUserId();
            ViewBag.esteAdmin = false;
            if (User.IsInRole("Administrator"))
            {
                ViewBag.esteAdmin = true;
            }
            ViewBag.img = img;
            if (searched || !likesPresent)
            {
                ViewBag.images = images;
            }
            else
            {
                ViewBag.images = imagesArray;
            }
            return View();
        }

        public ActionResult Show(int id)
        {
            Photo image = db.Photos.Find(id);
            ViewBag.Image = image;
            ViewBag.Category = image.Category;
            ViewBag.afisareButoane = false;
            if (image.UserId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
            {
                ViewBag.afisareButoane = true;
            }
            ViewBag.esteAdmin = User.IsInRole("Administrator");
            ViewBag.utilizatorCurent = User.Identity.GetUserId();

            var comments = from comment in db.Comments
                           where comment.IdPhoto == id && comment.Status == "Approved"
                           orderby comment.createdDate
                           select comment;

            ViewBag.Comments = comments;

            var userId = User.Identity.GetUserId();
            var like = from likes in db.Likes
                       where likes.PhotoId == id && likes.UserId == userId
                       orderby likes.createdDate descending
                       select likes;

            var photoLikes = from likes in db.Likes
                             where likes.PhotoId == image.Id
                             orderby likes.UserId, likes.createdDate descending
                             select likes;

            int nrLikes = 0;
            if(photoLikes.Count() > 0)
            {
                var currentLike = photoLikes.First();
                var isFirst = true;
                foreach (var photoLike in photoLikes)
                {
                    if(isFirst == true)
                    {
                        isFirst = false;
                        if (photoLike.isLike)
                            nrLikes++;
                    }
                    else
                    {
                        if(photoLike.UserId != currentLike.UserId)
                        {
                            currentLike = photoLike;
                            if (photoLike.isLike)
                                nrLikes++;
                        }
                    }
                }
            }

            ViewBag.likes = nrLikes;
            
            if (like.Count() > 0 && like.First().isLike)
            {
                ViewBag.like = like.First();
            }
            else ViewBag.like = null;

            if (userId != null)
            {
                try
                {
                    var visualization = from statistic in db.Statistics
                                        where statistic.UserId == userId && statistic.CategoryId == image.IdCategory
                                        select statistic;
                    visualization.First().ViewCounter = visualization.First().ViewCounter + 1;

                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    return View(image);
                }
            }

            return View(image);

        }

        public ActionResult Like(int id)
        {
            _userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var userId = User.Identity.GetUserId();
            if (userId == null)
            {
                return RedirectToAction("Login", "AccountControler");
            }
            else
            {
                try
                {
                    var catId = from photos in db.Photos
                                where photos.Id == id
                                select photos.IdCategory;
                    Like newLike = new Like();
                    newLike.PhotoId = id;
                    newLike.UserId = userId;
                    newLike.CategoryId = catId.First();
                    newLike.createdDate = DateTime.Now;
                    newLike.isLike = true;

                    var likes = from photoLikes in db.Likes
                                where photoLikes.PhotoId == id && photoLikes.UserId == userId
                                orderby photoLikes.createdDate descending
                                select photoLikes;
                    if (likes.Count() > 0 && likes.First().isLike)
                    {
                        newLike.isLike = false;
                    }
                    db.Likes.Add(newLike);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    return Redirect(Request.UrlReferrer.ToString()); ;
                }
            }

            return Redirect(Request.UrlReferrer.ToString()); ;
        }

        [Authorize(Roles = "User, Administrator")]
        public ActionResult AddCom()
        {
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "User, Administrator")]
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddCom(Comment comment)
        {
            if (!String.IsNullOrEmpty(comment.Content))
            {
                var userId = User.Identity.GetUserId();
                if (userId == null)
                {
                    return RedirectToAction("Login", "Account");
                }
                else comment.UserId = userId;
                comment.Status = "Pending";
                comment.createdDate = DateTime.Now;
                db.Comments.Add(comment);
                /*string query = "INSERT INTO Comments (IdPhoto, Status, UserId, Content)  OUTPUT INSERTED.ID VALUES (" + 
                    comment.IdPhoto.ToString() + ", '" + comment.Status + "', '" + comment.UserId + "', '" + comment.Content + "')";
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDb)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\aspnet-SmartPhotos-20190330121938.mdf;Initial Catalog=aspnet-SmartPhotos-20190330121938;Integrated Security=True;");
                int nr = 1;
                try
                {
                    con.Open();
                    nr = 2;
                    SqlCommand com = new SqlCommand(query, con);
                    nr = 3;
                    int id = (int)com.ExecuteScalar();
                    nr = 4;
                }
                catch(Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                    return Redirect("Index/"+nr);
                }
                finally
                {
                    con.Close();
                }*/
                db.SaveChanges();
                return Redirect("Show/" + comment.IdPhoto.ToString());
            }
            return Redirect("Show/" + comment.IdPhoto.ToString());
        }

        [Authorize(Roles = "User,Administrator")]
        public ActionResult Create()
        {
            PhotoViewModel photo = new PhotoViewModel();
            //preluam lista de categorii din metoda GetAllCategories()
            photo.Categories = GetAllCategories();
            //preluam id-ul userul curent
            photo.UserId = User.Identity.GetUserId();
            return View(photo);
        }

        [NonAction]
        public IEnumerable<SelectListItem> GetAllCategories()
        {
            // generam o lista goala
            var selectList = new List<SelectListItem>();
            // Extragem toate categoriile din baza de date
            var categories = from cat in db.Categories select cat;
            // iteram prin categorii
            foreach (var category in categories)
            {
                // Adaugam in lista elementele necesare pentru dropdown
                selectList.Add(new SelectListItem
                {
                    Value = category.Id.ToString(),
                    Text = category.Nume.ToString()
                });
            }
            // returnam lista de categorii
            return selectList;
        }
        [NonAction]
        public void report(int id)
        {
            var ids = from photo in db.Photos
                        where photo.Id != id
                        orderby photo.Id
                        select photo.Id;
            foreach(var idPhoto in ids)
            {
                var likes = from like in db.Likes
                            where like.PhotoId == id
                            select like;
                foreach(var like in likes)
                {
                    like.PhotoId = idPhoto;
                }
                break;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User,Administrator")]
        public ActionResult Create(PhotoViewModel model)
        {
            model.Categories = GetAllCategories();
            try
            {
                var validImageTypes = new string[]
                {
                    "image/gif",
                    "image/jpeg",
                    "image/pjpeg",
                    "image/png"
                };

                if (model.ImageUpload == null || model.ImageUpload.ContentLength == 0)
                {
                    ModelState.AddModelError(ManageController.errors[0].Item1, ManageController.errors[0].Item2);
                }
                else if (model.ImageUpload.ContentLength>5000000)
                {
                    ModelState.AddModelError(ManageController.errors[1].Item1, ManageController.errors[1].Item2);
                }
                

                if (ModelState.IsValid)
                {
                    var image = new Photo
                    {
                        Title = model.Title,
                        AltText = model.AltText,
                        Caption = model.Caption,
                        IdCategory = model.IdCategory,
                        UserId = model.UserId
                    };

                    if (model.ImageUpload != null && model.ImageUpload.ContentLength > 0)
                    {
                        var uploadDir = "~/uploads";
                        var numeImg = model.ImageUpload.FileName;
                        var imagePath = Path.Combine(Server.MapPath(uploadDir), numeImg);
                        var imageUrl = Path.Combine(uploadDir, numeImg);
                        model.ImageUpload.SaveAs(imagePath);
                        image.ImageUrl = imageUrl;
                    }

                    db.Photos.Add(image);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                return View(model);
            }

            return View(model);
        }

        [Authorize(Roles = "User,Administrator")]
        public ActionResult Edit(int id)
        {
            var image = db.Photos.Find(id);
            if (image == null)
            {
                return new HttpNotFoundResult();
            }
            if (image.UserId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
            {
                var model = new PhotoViewModel
                {
                    Title = image.Title,
                    AltText = image.AltText,
                    Caption = image.Caption,
                    IdCategory = image.IdCategory,
                    UserId = image.UserId
                };
                model.Categories = GetAllCategories();
                return View(model);
            }
            else
            {
                TempData["message"] = ManageController.errors[3].Item2;
                return RedirectToAction("Index");
            }
        }

        [HttpPut]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User,Administrator")]
        public ActionResult Edit(int id, PhotoViewModel model)
        {
            var validImageTypes = new string[]
            {
                "image/gif",
                "image/jpeg",
                "image/pjpeg",
                "image/png"
            };

            /*if (model.ImageUpload == null || model.ImageUpload.ContentLength == 0 || !validImageTypes.Contains(model.ImageUpload.ContentType))
            {
                ModelState.AddModelError("ImageUpload", "Please choose either a GIF, JPG or PNG image.");
            }*/


            if (ModelState.IsValid)
            {
                var image = db.Photos.Find(id);

                if (image == null)
                {
                    return new HttpNotFoundResult();
                }

                if (model.UserId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
                {
                    if (TryUpdateModel(image))
                    {
                        image.Title = model.Title;
                        image.AltText = model.AltText;
                        image.Caption = model.Caption;
                        image.IdCategory = model.IdCategory;

                        /*if (model.ImageUpload != null && model.ImageUpload.ContentLength > 0)
                        {
                            var uploadDir = "~/uploads";
                            var imagePath = Path.Combine(Server.MapPath(uploadDir), model.ImageUpload.FileName);
                            var imageUrl = Path.Combine(uploadDir, model.ImageUpload.FileName);
                            model.ImageUpload.SaveAs(imagePath);
                            image.ImageUrl = imageUrl;
                        }*/

                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    TempData["message"] = ManageController.errors[3].Item2;
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }
        [HttpDelete]
        [Authorize(Roles = "User, Administrator")]
        public ActionResult Delete(int id)
        {

            Photo image = db.Photos.Find(id);

            if (image.UserId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
            {
                report(id);
                db.Photos.Remove(image);
                db.SaveChanges();
                TempData["message"] = "Imaginea a fost stearsa!";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["message"] = ManageController.errors[4].Item2;
                return RedirectToAction("Index");
            }

        }
    }
}