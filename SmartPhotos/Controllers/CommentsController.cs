﻿using Microsoft.AspNet.Identity;
using SmartPhotos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartPhotos.Controllers
{
    public class CommentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        [Authorize(Roles = "User,Administrator")]
        public ActionResult Index()
        {
            ViewBag.userCurent = User.Identity.GetUserId();
            if (User.IsInRole("Administrator"))
            {
                var comments = from comment in db.Comments
                               orderby comment.createdDate
                               select comment;
                foreach (var i in comments)
                {
                    i.Photo = db.Photos.Find(i.IdPhoto);
                }
                ViewBag.Comments = comments;
                ViewBag.esteAdmin = true;
            }
            else
            {
                var id = User.Identity.GetUserId();
                var comments = from comment in db.Comments
                               where comment.Status == "Pending"
                               orderby comment.createdDate
                               select comment;

                foreach (var i in comments)
                {
                    i.Photo = db.Photos.Find(i.IdPhoto);
                }

                ViewBag.Comments = comments;
                ViewBag.esteAdmin = false;
            }
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            return View();
        }

        
        [Authorize(Roles = "User,Administrator")]
        public ActionResult Edit(int id)
        {
            Comment comment = db.Comments.Find(id);
            ViewBag.Comment = comment;
            ViewBag.esteAdmin = false;
            if (User.IsInRole("Administrator"))
            {
                ViewBag.esteAdmin = true;
            }
            ViewBag.currentUser = User.Identity.GetUserId();
            return View(comment);
        }

        [HttpPut]
        [Authorize(Roles = "User,Administrator")]
        public ActionResult Edit(int id, Comment requestComment)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Comment comment = db.Comments.Find(id);
                    if (TryUpdateModel(comment))
                    {
                        comment.Content = requestComment.Content;
                        db.SaveChanges();
                        TempData["Message"] = "Commentul a fost modificat!";
                    }
                    return RedirectToAction("Show", "Photo", new { Id = comment.IdPhoto });
                }
                else
                {
                    return View(requestComment);
                }
            }
            catch (Exception e)
            {
                return View(requestComment);
            }
        }

        [HttpPut]
        [Authorize(Roles = "User,Administrator")]
        public ActionResult Approve(int id)
        {
            try
            {
                Comment comment = db.Comments.Find(id);
                if (TryUpdateModel(comment))
                {
                    comment.Status = "Approved";
                    db.SaveChanges();
                    TempData["Message"] = "Commentul a fost aprobat!";
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }

        [HttpPut]
        [Authorize(Roles = "User,Administrator")]
        public ActionResult Decline(int id)
        {
            try
            {
                Comment comment = db.Comments.Find(id);
                if (TryUpdateModel(comment))
                {
                    comment.Status = "Declined";
                    db.SaveChanges();
                    TempData["Message"] = "Commentul a fost respins!";
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }

        [HttpDelete]
        [Authorize(Roles = "User, Administrator")]
        public ActionResult Delete(int id)
        {

            Comment comment = db.Comments.Find(id);

            if (comment.UserId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
            {
                db.Comments.Remove(comment);
                db.SaveChanges();
                TempData["message"] = "Comentariul a fost sters!";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["message"] = "Nu aveti dreptul sa stergeti un comentariu care nu va apartine!";
                return RedirectToAction("Index");
            }

        }
    }
}