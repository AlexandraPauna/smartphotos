﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartPhotos.Models
{
    public class Like
    {
        [Key]
        public int LikeId { get; set; }

        [Required]
        public int PhotoId { get; set; }
        public virtual Photo Photo { get; set; }
        public ICollection<Photo> Photos { get; set; }

        [Required]
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        [Required]
        public int CategoryId { get; set; }

        public DateTime createdDate { get; set; }

        public bool isLike { get; set; }
    }
}