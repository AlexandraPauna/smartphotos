﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartPhotos.Models
{
    public class PhotoViewModel
    {
        [Key]
        public string Id { get; set; }

        [Required]
        [MaxLength(10)]
        public string Title { get; set; }

        public string AltText { get; set; }

        [Required]
        [DataType(DataType.Html)]
        public string Caption { get; set; }

        [Required]
        public int IdCategory { get; set; }

        public int IdAlbum { get; set; }

        public IEnumerable<SelectListItem> Albums { get; set; }

        public IEnumerable<SelectListItem> Categories { get; set; }

        public virtual Category Category { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        [NotMapped]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase ImageUpload { get; set; }
    }
}