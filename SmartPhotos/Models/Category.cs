﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartPhotos.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nume { get; set; }

        //public virtual ICollection<Photo> Photos { get; set; }
    }
}